from core.task import TaskBranch
from inference.analytics import Analytics
from network.connective import Connective
from presentation.gui import GUI
#from .network import NatsBridge

import asyncio


class NetworkTaskBranch(TaskBranch):
    def __init__(self, argv, *args, **kwargs):
        super().__init__(argv, *args, **kwargs)
        self._connective = None
        self._adaptor = None

    def main(self):
            self._connective = Connective("nats://192.168.1.16:4222") # nats://192.168.1.16:4222
            self._adaptor = NatsBridge(self._connective)

            self._connective.connect()
            self._adaptor.start()

            self._connective.run()
            self._connective.disconnect()

    def exit(self):
        self._connective.stop()


class InferenceTaskBranch(TaskBranch):
    def __init__(self, argv, *args, **kwargs):
        super().__init__(argv, *args, **kwargs)
        self._analytics = []
        self._loop = None

    def main(self):
        self._loop = asyncio.new_event_loop()
        self._analytics = Analytics(self.argv, self.args, self.kwargs)
        self._analytics.assign()
        self._loop.run_forever()

    def exit(self):
        self._loop.call_soon_threadsafe(self._loop.stop)


class PresentationTaskBranch(TaskBranch):
    def __init__(self, argv, *args, **kwargs):
        super().__init__(argv, *args, **kwargs)
        self._gui = GUI(argv, *args, **kwargs)

    def main(self):
        self._gui.exec()

    def exit(self):
        self._gui.quit()
