import json


class ConnectiveBridge:
    def __init__(self, connective=None, analytics=None):
        self._connective = connective

    @property
    def connective(self):
        return self._connective

    def start(self):
        self._connective.subscribe(method="dx.registered", callback=self.on_dx_registered)
        self._connective.subscribe(method="analytics.assign", callback=self.on_analytics_assign)
        self._connective.subscribe(method="analytics.update", callback=self.on_analytics_update)
        self._connective.subscribe(method="analytics.stop", callback=self.on_analytics_stop)

    def dx_register(self, msg):
        self._connective.publish(method="dx.register", payload=json.dumps(msg).encode())

    def analytics_state_changed(self, msg):
        self._connective.publish(method="analytics.state.changed", payload=json.dumps(msg).encode())

    def on_dx_registered(self, msg):
        jo = json.loads(msg.data.decode())

    def on_analytics_assign(self, msg):
        jo = json.loads(msg.data.decode())

    def on_analytics_update(self, msg):
        jo = json.loads(msg.data.decode())

    def on_analytics_stop(self, msg):
        jo = json.loads(msg.data.decode())
