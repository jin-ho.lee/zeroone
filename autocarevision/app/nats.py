from network.connective import Connective

from PySide2.QtCore import Property, Signal, QObject, Slot


class GuiConnection:
    def __init__(self):
        pass

    def start(self):
        self._connective.subscribe(method="stream.list.get", callback=self.on_stream_list_get)

    def on_stream_list_get(self, msg):
        print(msg)


class NatsAdaptor(QObject):
    def __init__(self, uri: str = ""):
        QObject.__init__(self)
        self._nats = Connective(uri=uri)

    def _protocol(self):
        return self._rest.protocol

    def _uri(self):
        return self._rest.uri

    def _uri_with_basic_auth(self):
        return self._rest.uri_with_basic_auth

    def _servers(self):
        return self._rest.get("ec2/getMediaServersEx")

    def _cameras(self):
        return self._rest.get("ec2/getCamerasEx")

    serversChanged = Signal()
    camerasChanged = Signal()

    protocol = Property(str, _protocol)
    uri = Property(str, _uri)
    uri_with_basic_auth = Property(str, _uri_with_basic_auth)
    servers = Property(str, _servers, notify=serversChanged)
    cameras = Property(str, _cameras, notify=camerasChanged)
