[property]
gpu-id=0
net-scale-factor=0.0039215697906911373
#0=RGB, 1=BGR
model-color-format=0
model-engine-file=../models/Primary_Detector/sorest_int8_batch4.engine
labelfile-path=../models/Primary_Detector/labels.txt
num-detected-classes=4
interval=0
gie-unique-id=1
process-mode=1
network-type=0
## 0=Group Rectangles, 1=DBSCAN, 2=NMS, 3= DBSCAN+NMS Hybrid, 4 = None(No clustering)
cluster-mode=4
maintain-aspect-ratio=1
parse-bbox-func-name=NvDsInferParseCustomYoloV5
custom-lib-path=../models/Primary_Detector/nvdsinfer_custom_impl_Yolo/libnvdsinfer_custom_impl_Yolo.so
## 0=FP32, 1=INT8, 2=FP16 mode
network-mode=1
int8-calib-file=../models/Primary_Detector/Int8CalibrationTable_sorest_int8.engine512x0_125
batch-size=1
#scaling-filter=1
#scaling-compute-hw=0
#infer-dims=3;512;512
offsets=0.0;0.0;0.0
#workspace-size=1000000

[class-attrs-all]
pre-cluster-threshold=0.4
nms-iou-threshold=0.45
