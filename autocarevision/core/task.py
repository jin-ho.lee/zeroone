import sys
from abc import abstractmethod
import asyncio
import concurrent.futures
import re
import threading


class TaskManager(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super().__new__(cls)
        return cls._instance


class TaskDelegator:
    def __init__(self, tasks: list = None):
        self._tasks = {}
        self._thread_executor = None
        self._asyncio_executor = None

        # initialize task executor
        asyncio_tasks = {}
        thread_tasks = {}
        for task in tasks:
            key = re.sub(r"([A-Z])", r" \1", task.__class__.__name__).split()[0].lower()
            value = {"task": task, "result": 0}
            if task.mode is TaskBranch.Thread:
                thread_tasks[key] = value
            elif task.mode is TaskBranch.Asyncio:
                asyncio_tasks[key] = value
            else:
                print("unsupported task mode: mode={}", task.mode)
            self._tasks[key] = value
            task.parent = self
        self._thread_executor = _ThreadExcutor(tasks=thread_tasks)
        self._asyncio_executor = _AsyncioExecutor(tasks=asyncio_tasks)

    def tasks(self, name):
        return self._tasks[name]["task"] if name in self._tasks else None

    def exec(self):
        futures = self._thread_executor.exec()
        self._asyncio_executor.exec_until_completed()   # blocking here

        self.terminate()
        self._thread_executor.as_completed(futures)

    def terminate(self):
        self._asyncio_executor.terminate()
        self._thread_executor.terminate()


class TaskBranch:
    Asyncio = 0
    Thread = 1

    def __init__(self, argv, *args, mode=Thread, **kwargs):
        self._mode = mode
        self._parent = None

    def __repr__(self):
        return "<{}:{}[mode={}]>".format(self.__class__.__name__, threading.get_native_id(), self._mode)

    @property
    def mode(self):
        return self._mode

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent):
        self._parent = parent


    @abstractmethod
    def main(self):
        raise NotImplementedError("derived class doesn't implemented")

    @abstractmethod
    def exit(self):
        raise NotImplementedError("derived class doesn't implemented")


class _AsyncioExecutor:
    def __init__(self, tasks=None, loop=asyncio.get_event_loop()):
        self._tasks = tasks
        self._loop = loop

    def exec_until_completed(self):
        self._loop.run_until_complete(
            asyncio.gather(*[_AsyncioExecutor._bootup(obj["task"]) for obj in self._tasks.values()])
        )

    def terminate(self):
        for obj in self._tasks.values():
            obj["task"].exit()

    @classmethod
    async def _bootup(cls, task):
        task.main()


class _ThreadExcutor(concurrent.futures.ThreadPoolExecutor):
    def __init__(self, tasks=None):
        super().__init__(max_workers=len(tasks))
        self._tasks = tasks

    def exec(self):
        return {self.submit(_ThreadExcutor._bootup, obj["task"]): obj for obj in self._tasks.values()}

    def as_completed(self, futures):
        for future in concurrent.futures.as_completed(futures):
            try:
                futures[future]["result"] = future.result()
            except:
                print("error while running {}: {}".format(future, sys.exc_info()[0]))
                futures[future]["result"] = sys.exc_info()[0]

    def terminate(self):
        for obj in self._tasks.values():
            obj["task"].exit()

    @classmethod
    def _bootup(cls, task):
        task.main()
