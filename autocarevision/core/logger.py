import logging


logger = None


def init_logger(logfile: str = str(), loglevel: str = str()):
    def _to_logging_level(level):
        if level == "debug":
            return logging.DEBUG
        if level == "info":
            return logging.INFO
        if level == "warning":
            return logging.WARNING
        if level == "error":
            return logging.ERROR
        if level == "critical":
            return logging.CRITICAL
        return logging.ERROR

    global logger
    logger = logging.getLogger("dx")
    if logfile:
        handler = logging.FileHandler(logfile)
    else:
        handler = logging.StreamHandler()
    if loglevel:
        handler.setLevel(_to_logging_level(loglevel))
    handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d %(name)s %(levelname)-8s %(message)s",
                                           datefmt="%I:%M:%S"))
    logger.addHandler(handler)
