import QtQuick 2.15
import QtQuick.Controls 2.15


Item {
    id: root

    anchors.topMargin: 5
    anchors.bottomMargin: 5
    anchors.leftMargin: 10
    anchors.rightMargin: 10

    property var clicked: nullptr

    property var cellWidth: root.width - (anchors.leftMargin + anchors.rightMargin)
    property var cellHeight: root.height - (anchors.topMargin + anchors.bottomMargin)

    FocusScope {
        anchors.verticalCenter: parent.verticalCenter
        width: root.cellWidth
        height: root.cellHeight

        ClickableButton {
            id: button

            width: height * 1.5
            height: parent.height

            text: "Add"
            textSize: 13

            onSelected: root.clicked(text)
        }

        TextField {
            id: field

            anchors.verticalCenter: parent.verticalCenter
            anchors.left: button.right
            anchors.right: parent.right
            anchors.leftMargin: 10
            height: parent.height

            font.pixelSize: 13
            placeholderText: qsTr("Enter Server Address")
        }
    }
}