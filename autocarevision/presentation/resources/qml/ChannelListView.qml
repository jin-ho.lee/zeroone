import QtQuick 2.15


Item {
    id: root
    objectName: "qml_ChannelListView"

    property var originModel: nullptr
    signal itemClicked(var item)
    signal itemDoubleClicked(var item)
    signal controlClicked(var action)

    Background {
        id: background

        anchors.fill: parent

        source: "images/logo.png"
        text: "CHANNEL LIST"
    }

    ChannelList {
        id: list

        anchors.top: parent.top
        anchors.bottom: hdivider.top
        anchors.left: parent.left
        anchors.right: parent.right

        clicked: root.itemClicked
        doubleClicked: root.itemDoubleClicked

        Component.onCompleted: {
            var servers = JSON.parse(mediaserver.servers)
            servers.forEach(server => {
                list.model.append({
                    "type": "server",
                    "image": "images/018-database.svg",
                    "id": server.id,
                    "name": server.name
                })
            })

            var cameras = JSON.parse(mediaserver.cameras)
            cameras.forEach(camera => {
                list.model.append({
                    "type": "camera",
                    "image": "images/085-signal.svg",
                    "id": camera.id,
                    "name": camera.name
                })
            })
        }
    }

    Divider {
        id: hdivider

        height: 10
        anchors.bottom: control.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    ChannelListControl {
        id: control

        height: 40
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        clicked: controlClicked
    }
}