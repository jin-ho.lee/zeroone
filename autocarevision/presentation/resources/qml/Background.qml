import QtQuick 2.15


Rectangle {
    id: root

    color: "#EBECED"

    property alias source: img.source
    property alias text: txt.text


    Image {
        id: img

        anchors.centerIn: parent

        opacity: 0.5
    }

    Text {
        id: txt

        anchors.horizontalCenter: img.horizontalCenter
        anchors.top: img.bottom
        anchors.topMargin: -20

        color: "#FFFFFF"
        font.bold: true
        font.italic : true
        opacity: 0.5
    }
}