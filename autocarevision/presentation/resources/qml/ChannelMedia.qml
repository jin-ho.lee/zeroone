import QtQuick 2.15


FocusScope {
    id: root

    property var modelData: null
    signal closeIconClicked()


    Rectangle {
        id: viewer

        width: parent.width * 0.9
        height: toolbar.height + player.height
        anchors.centerIn: parent

        color: "transparent"
        border.color: "#CECDCA"
        border.width: 1
 
        ChannelMediaPlayer {
            id: player

            width: toolbar.width
            anchors.top: toolbar.bottom

            mediaSource: modelData.media_uri
        }

        ChannelMediaToolBar {
            id: toolbar

            width: viewer.width
            height: 30
            anchors.top: viewer.top
            anchors.horizontalCenter: viewer.horizontalCenter

            iconSource: "images/085-signal.svg"
            titleText: modelData.name
            titleTextSize: 14
            closeIconClicked: root.closeIconClicked
        }
    }
}