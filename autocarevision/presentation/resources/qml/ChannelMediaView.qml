import QtQuick 2.15


Item {
    id: root
    objectName: "qml_ChannelMediaView"


    Background {
        id: background

        anchors.fill: parent

        source: "images/logo.png"
        text: "CHANNEL MEDIA"
    }

    ChannelMediaGrid {
        id: grid

        anchors.fill: parent
    }

    function requestAddChannel(channel) {
        function _getChannelRTSPMediaUri(id) {
            return mediaserver.uri_with_basic_auth.replace(mediaserver.protocol, "rtsp") +
                   "/" +
                   id.replace(/[{}]/g, '')
        }

        var cameras = JSON.parse(mediaserver.cameras)
        for (var i = 0; i < cameras.length; i++) {
            if (cameras[i].id == channel.id) {
                grid.model.append({
                    "name": cameras[i].name,
                    "id": cameras[i].id,
                    "url": cameras[i].url,
                    "media_uri": _getChannelRTSPMediaUri(cameras[i].id)
                })
                break
            }
        }
    }
}