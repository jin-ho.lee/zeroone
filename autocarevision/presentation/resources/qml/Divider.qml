import QtQuick 2.15


Item {
    id: root

    property alias lineColor : line.color
    property var direction: "horizontal"

    Rectangle {
        id: line
        width: direction == "horizontal" ? parent.width * 0.98 : 1
        height: direction == "vertical" ? parent.height * 0.98 : 1
        anchors.centerIn: parent

        color: "#CECDCA"
    }
}