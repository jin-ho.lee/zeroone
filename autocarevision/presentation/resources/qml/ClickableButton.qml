import QtQuick 2.15

Item {
    id: root

    property alias text: txt.text
    property alias textSize: txt.font.pixelSize
    signal selected(var action)


    Rectangle {
        id: background

        anchors.fill: parent

        color: "#EBECED"
        border.color: "#CECDCA"
        radius: 6

        function setHover(hovered) {
            color = hovered ? "#CECDCA" : "#EBECED"
        }

        function setPress(press) {
            color = press ? "#9B9A97" : "#CECDCA"
        }
    }

    Text {
        id: txt

        anchors.fill: parent

        color: "#37352F"
        antialiasing: true
        minimumPixelSize: 8
        fontSizeMode: Text.Fit
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        function setPress(press) {
            font.bold = press
        }
    }

    MouseArea {
        anchors.fill: parent

        hoverEnabled: true
        onEntered: background.setHover(true)
        onExited: background.setHover(false)
        onPressed: background.setPress(true)
        onReleased: background.setPress(false)
        onClicked: root.selected(txt.text)
    }
}