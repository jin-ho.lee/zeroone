import QtQuick 2.15

ListView {
    id: root

    anchors.topMargin: 10
    anchors.bottomMargin: 10
    anchors.leftMargin: 10
    anchors.rightMargin: 10

    property var clicked: nullptr
    property var doubleClicked: nullptr
    property var cellHeight: 24

    interactive: false
    spacing: 2
    model: ListModel {}

    delegate: ChannelItem {
        width: root.width
        height: cellHeight

        modelData: model
        clicked: root.clicked
        doubleClicked: root.doubleClicked
    }
}