import QtQuick 2.15

Item {
    id: root

    property alias iconSource: icon.source
    signal clicked()


    Rectangle {
        id: background

        width: parent.width * 0.6
        height: parent.height * 0.6
        anchors.centerIn: parent

        color: "#EBECED"
        border.color: "#CECDCA"
        radius: 6

        function setHover(hovered) {
            color = hovered ? "#CECDCA" : "#EBECED"
        }

        function setPress(press) {
            color = press ? "#9B9A97" : "#CECDCA"
        }
    }

    Image {
        id: icon

        width: parent.width * 0.4
        height: parent.height * 0.4
        anchors.centerIn: parent

        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter
    }

    MouseArea {
        anchors.fill: parent

        hoverEnabled: true
        onEntered: background.setHover(true)
        onExited: background.setHover(false)
        onPressed: background.setPress(true)
        onReleased: background.setPress(false)
        onClicked: root.clicked()
    }
}