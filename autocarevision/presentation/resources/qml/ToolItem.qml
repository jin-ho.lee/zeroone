import QtQuick 2.15

FocusScope {
    id: root

    property var modelData: null
    property var clicked: null

    FocusScope {
        id: inner

        anchors.fill: parent
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        Image {
            id: icon

            anchors.fill: parent
            height: 35

            source: modelData.icon
            mipmap: true
        }

        Text {
            id: txt

            anchors.top: icon.bottom

            text: modelData.name
            antialiasing: true
            font.pixelSize: 10
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
}