import QtQuick 2.15
import QtMultimedia 5.13


FocusScope {
    id: root

    property var mediaSource: null

    height: errorImage.visible ? (errorImage.height + errorImage.anchors.topMargin * 2) : output.contentRect.height


    MediaPlayer {
        id: player

        autoLoad: false
        loops: Audio.Infinite
        autoPlay: true

        onError: errorImage.setVisible(true)
    }

    VideoOutput {
        id: output
        width: parent.width
        anchors.centerIn: parent

        source: player
    }

    Image {
        id: errorImage

        width: parent.width / 4
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 10

        visible: false
        source: "images/091-warning.svg"
        mipmap: true
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter

        function setVisible(vis) {
            visible = vis
        }
    }

    onMediaSourceChanged: {
        player.source = "gst-pipeline: uridecodebin uri=%1 ! qtvideosink".arg(mediaSource)
    }
}