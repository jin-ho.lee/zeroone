import QtQuick 2.15

GridView {
    id: root

    model: ListModel {}
    delegate: ChannelMedia {
        width: root.cellWidth
        height: root.cellHeight

        modelData: model
        
        onCloseIconClicked: root.model.remove(index, 1)
    }

    add: transition
    remove: transition
    displaced: transition


    Transition {
        id: transition
        ParallelAnimation {
            NumberAnimation { property: "x"; easing.type: Easing.OutQuart; duration: 300}
            NumberAnimation { property: "y"; easing.type: Easing.OutQuart; duration: 300}
            NumberAnimation { property: "width"; easing.type: Easing.OutQuart; duration: 300}
            NumberAnimation { property: "height"; easing.type: Easing.OutQuart; duration: 300}
        }
    }

    Connections {
        target: model
        function onCountChanged() {
            root.updateCellSize()
        }
    }


    Component.onCompleted: root.updateCellSize()
    onWidthChanged: root.updateCellSize()
    onHeightChanged: root.updateCellSize()


    function updateCellSize() {
        if (model.count <= 1) {
            root.cellWidth = root.width
            root.cellHeight = root.height
        } else if (model.count == 2) {
            root.cellWidth = root.width / 2
            root.cellHeight = root.height
        } else if (model.count >= 3) {
            root.cellWidth = root.width / 2
            root.cellHeight = root.height / 2
        }
    }
}