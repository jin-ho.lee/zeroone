import QtQuick 2.15
import QtQuick.Window 2.15


Window {
    id: primaryWindow
    objectName: "primaryWindow"

    width: system.screenSize.width * 2 / 3
    height: system.screenSize.height * 2 / 3
    visible: true
    title: qsTr("autocarevision")

    signal loadCompleted()


    FocusScope {
        id: root

        anchors.fill: parent


        // Visual layer components containing GUI resources
        Background {
            id: background
            anchors.fill: parent
        }

        ToolListView {
            id: toolListView

            width: 50
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
        }

        Divider {
            id: div1

            width: 10
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: toolListView.right

            direction: "vertical"
        }

        ChannelListView {
            id: channelListView

            width: Math.floor(parent.width/5)
            anchors.left: div1.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            originModel: root.originModel
        }

        Divider {
            id: div2

            width: 10
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: channelListView.right

            direction: "vertical"
        }

        ChannelMediaView {
            id: channelMediaView

            anchors.left: div2.right
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            Connections {
                target: channelListView
                function onItemDoubleClicked(item) {
                    channelMediaView.requestAddChannel(item)
                }
            }
        }
    }

    Connections {
        target: messageBridge
        function onReceived(msg) {
            console.log("msg: " + msg)
        }
    }
}