import QtQuick 2.15


FocusScope {
    id: root

    property var iconSource: null
    property alias titleText: toolbar.titleText
    property alias titleTextSize: toolbar.titleTextSize
    property var closeIconClicked: null

    Rectangle {
        id: toolbar

        property alias titleText: title.text
        property alias titleTextSize: title.font.pixelSize
        property alias closeIcon: close

        anchors.fill: parent

        color: "#EBECED"


        function setHover(hovered) {
            color = hovered ? "#CECDCA" : "#EBECED"
        }

        Image {
            id: icon

            width: height
            height: parent.height * 0.5
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: 4

            source: root.iconSource
            mipmap: true
        }

        Text {
            id: title

            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: icon.right
            anchors.leftMargin: 10
            
            antialiasing: true
            minimumPixelSize: 8
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
        }

        ClickableIcon {
            id: close

            width: height
            height: parent.height
            anchors.top: parent.top
            anchors.right: parent.right

            iconSource: "images/020-close.svg"

            onClicked: root.closeIconClicked()
        }
    }

    Rectangle {
        id: upperLine

        height: 1
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        color: "#CECDCA"
    }

    Rectangle {
        id: lowerLine

        height: 1
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        color: "#CECDCA"
    }
}