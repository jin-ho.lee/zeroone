import QtQuick 2.15


ListView {
    id: root

    anchors.topMargin: 20

    spacing: 4

    model: ListModel {
        ListElement {
            icon: "images/009-coding.svg"
            name: "streaming"
        }
    }

    delegate: ToolItem {
        width: root.width
        height: width

        modelData: model
    }
}