import QtQuick 2.15


FocusScope {
    id: root

    property var modelData: nullptr
    property var clicked: nullptr
    property var doubleClicked: nullptr

    Rectangle {
        id: focused

        anchors.fill: parent
        visible: false

        color: "#21697f"
        opacity: 0.8
        radius: 5
    }

    Image {
        id: icon

        width: height
        height: parent.height
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 4

        source: modelData.image
        mipmap: true
    }
    Text {
        id: nameText

        height: parent.height
        anchors.left: icon.right
        anchors.right: parent.right
        anchors.top: parent.top

        text: modelData.name
        color: "#37352F"
        antialiasing: true
        font.pixelSize: 13
        font.bold: true
        minimumPixelSize: 10
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        leftPadding: 10
    }

    MouseArea {
        anchors.fill: parent

        hoverEnabled: true
        onEntered: focused.visible = true
        onExited: focused.visible = false
        onDoubleClicked: root.doubleClicked(modelData)
    }
}