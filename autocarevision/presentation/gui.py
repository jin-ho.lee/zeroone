from presentation import component

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
import os
import sys


class GUI:
    def __init__(self, argv, *args, **kwargs):
        self._app = QGuiApplication(argv)
        self._engine = QQmlApplicationEngine()
        self._system = None
        self._message_bridge = None

        # initialize
        self._system = component.System(self._app, self._engine)
        self._engine.rootContext().setContextProperty('system', self._system)
        self._message_bridge = component.MessageBridge(self._app, self._engine)
        self._engine.rootContext().setContextProperty('messageBridge', self._message_bridge)

        self._engine.load(os.path.join(os.path.dirname(__file__), "resources/qml/main.qml"))
        if not self._engine.rootObjects():
            sys.exit(-1)

    def exec(self):
        # run application until quit
        self._app.exec_()

    def quit(self):
        self._app.quit()