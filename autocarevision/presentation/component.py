from PySide2.QtCore import Property, Signal, Slot, QObject, QSize


class System(QObject):
    def __init__(self, app=None, engine=None):
        QObject.__init__(self)
        self._app = app
        self._engine = engine

    def _screenSize(self):
        return self._app.primaryScreen().size()

    screenSizeChanged = Signal()

    screenSize = Property(QSize, _screenSize, notify=screenSizeChanged)


class MessageBridge(QObject):
    def __init__(self, app=None, engine=None):
        QObject.__init__(self)
        self._app = app
        self._engine = engine
        self.send_message.connect(self.on_message)

    @Slot(dict)
    def on_message(self, msg):
        self.received.emit(msg)

    send_message = Signal(dict)
    received = Signal(dict)
