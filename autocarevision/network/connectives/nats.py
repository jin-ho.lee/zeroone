import asyncio
import nats.aio.client


async def wait_for_stop(loop):
    while loop.is_running():
        await asyncio.sleep(0.0)


class Nats:
    def __init__(self, uri, loop=asyncio.new_event_loop(), **options):
        self._uri = uri
        self._client = nats.aio.client.Client()
        self._loop = loop

    @property
    def uri(self):
        return self._uri

    @property
    def loop(self):
        return self._loop

    @property
    def is_connected(self):
        return self._client.is_connected

    def connect(self):
        self._loop.run_until_complete(
            self._client.connect(self._uri,
                                 loop=self._loop,
                                 max_reconnect_attempts=1))

    def disconnect(self):
        self._loop.run_until_complete(self._client.close())

    def publish(self, method, payload):
        self._loop.run_until_complete(self._client.publish(method, payload.encode()))

    def subscribe(self, method, callback=None):
        self._loop.run_until_complete(self._client.subscribe(method, cb=callback))

    def run(self):
        self._loop.run_forever()

    def stop(self):
        self._loop.call_soon_threadsafe(self._loop.stop)
        #self._loop.run_until_complete(wait_for_stop(self._loop))  # assure to be safe closing the connective