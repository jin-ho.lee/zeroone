from network.connectives._mapping import CONNECTIVES

from urllib.parse import urlparse


__all__ = ['get_connective_proxy_by_uri']


def get_connective_proxy_by_uri(uri, **options):
    parsed = urlparse(uri)
    if parsed.scheme not in CONNECTIVES:
        raise IOError("unsupported uri: {}" % uri)

    module_name, name = CONNECTIVES[parsed.scheme]
    mod = __import__(module_name, None, None, ['__all__'])
    return getattr(mod, name)(uri, **options)


