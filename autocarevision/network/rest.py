import requests
from requests.auth import HTTPBasicAuth
from urllib.parse import urlparse


class RestHttp:
    def __init__(self, uri, username=None, password=None):
        self._uri = uri
        self._username = username
        self._password = password

    @property
    def protocol(self):
        return urlparse(self._uri).scheme

    @property
    def uri(self):
        return self._uri

    @property
    def uri_with_basic_auth(self):
        parsed = urlparse(self._uri)
        return "{}://{}:{}@{}".format(parsed.scheme, self._username, self._password, parsed.netloc)

    def get(self, url, headers=None, data=None):
        response = requests.get('{}/{}'.format(self._uri, url),
                                headers=headers,
                                data=data,
                                verify=False,
                                auth=HTTPBasicAuth(self._username, self._password))
        return response.text