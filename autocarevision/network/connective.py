import network.connectives


class Connective:
    def __init__(self, uri="", **options):
        self._proxy = network.connectives.get_connective_proxy_by_uri(uri, **options)

    def __getattr__(self, item):
        return getattr(self._proxy, item)

    def connect(self):
        self._proxy.connect()

    def disconnect(self):
        if self._proxy.is_connected:
            self._proxy.disconnect()

    def publish(self, method=None, payload=""):
        self._proxy.publish(method, payload)

    def subscribe(self, method, callback):
        self._proxy.subscribe(method, callback)

    def reply(self):
        pass

    def run(self):
        self._proxy.run()

    def stop(self):
        self._proxy.stop()
