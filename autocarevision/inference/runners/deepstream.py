from inference.analytics import Runner

import configparser
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

import sys
sys.path.append('/opt/nvidia/deepstream/deepstream/lib')

import pyds


class DeepStreamRunner(Runner):
    def __init__(self, **kwargs):
        Runner.__init__(self, **kwargs)
        self._pipeline = None

    def init(self):
        self._pipeline = DeepStreamPipeline()

    def config(self, model, job):
        self._pipeline.update(model, job)

    def run(self):
        self._pipeline.run()

    def stop(self):
        self._pipeline.stop()


class GstObject:
    def __init__(self, parent=None, context=None):
        self._parent = parent
        self._context = context
        self._properties = {}
        self._links = []

    @property
    def parent(self):
        return self._parent

    @property
    def context(self):
        return self._context

    @property
    def properties(self):
        return self._properties

    @property
    def links(self):
        return self._links

    @property
    def is_valid(self):
        return bool(self._context)

    def child_of_parent(self):
        if self._parent:
            self._parent.add(self)
        return self

    def set_property(self, key, value):
        if self._context:
            self._context.set_property(key, value)
        self._properties[key] = value

    def get_property(self, key):
        if self._context:
            return self._context.get_property(key)
        return self._properties.get(key)

    def link_to(self, obj):
        if self._context and obj.is_valid:
            self._context.link(obj.context)
        self._links.append(obj)

    def link_with(self, obj):
        obj.link_to(self)
        return self


class GstPipeline(GstObject):
    def __init__(self):
        GstObject.__init__(self, context=Gst.Pipeline())
        self._children = []

    @property
    def children(self):
        return self._children

    def add(self, obj):
        if self.is_valid and obj.is_valid:
            self.context.add(obj.context)
        self._children.append(obj)


class GstElement(GstObject):
    def __init__(self, parent, factory_name, name):
        GstObject.__init__(self, parent=parent, context=Gst.ElementFactory.make(factory_name, name))
        self._factory_name = factory_name
        self._name = name

    @property
    def factory_name(self):
        return self._factory_name

    @property
    def name(self):
        return self._name


class GstBin(GstObject):
    def __init__(self, parent, name):
        GstObject.__init__(self, parent=parent, context=Gst.Bin.new(name))
        self._name = name
        self._children = []

    @property
    def name(self):
        return self._name

    @property
    def children(self):
        return self._children

    def add(self, element):
        if self.is_valid and element.is_valid:
            Gst.Bin.add(self.context, element.context)
        self._children.append(element)


class DeepStreamPipeline(GstPipeline):
    def __init__(self):
        GstPipeline.__init__(self)

    def update(self, model, job):
        streammux = NvStreamMux(self).setup(model, job).child_of_parent()
        inference = InferBin(self).setup(model, job).link_with(streammux).child_of_parent()
        fakesink = FakeSink(self).setup(model, job).link_with(inference).child_of_parent()


class InferBin(GstBin):
    def __init__(self, pipeline, name=None):
        GstBin.__init__(self, pipeline, name if name else "infer-bin")

    def setup(self, model, job):
        for idx, e in enumerate(model.inferbin_structure):
            # TODO please use GstElement class
            element = GstElement(self, e["element_type"], e["element_name"])
            if e["element_type"] == "nvinfer":
                if idx is 0:
                    element.set_property("config-file-path", e["config-file-path"])
                    if job and len(job.stream) is not element.get_property("batch-size"):
                        print("Warning to overriding infer-config batch-size {} with number of sources {}".
                              format(element.get_property("batch-size"), len(job.stream)))
                        element.set_property("batch-size", len(job.stream))
            elif e["element_type"] == "nvtracker":
                config = configparser.ConfigParser()
                config.read(e["config-file-path"])
                config.sections()
                for key in config["tracker"]:
                    if key in ["tracker-width", "tracker-height", "gpu-id", "enable-batch-process", "enable-past-frame"]:
                        element.set_property(key, config.getint("tracker", key))
                    elif key in ["ll-lib-file", "ll-config-file"]:
                        element.set_property(key, config.get("tracker", key))
                    else:
                        print("Warning to read config of nvtracker due to unsupported key: key={}".format(key))
            elif e["element_type"] == "nvdsanalytics":
                self._nvdsanalytics_save_config_file(e["config-file-path"], model, job)
                element.set_property("config-file", e["config-file-path"])
            else:
                print("Warning to setup due to unsupported element: factory name={}, name={}".
                      format(e["element_type"], e["element_name"]))

            if idx is not 0:
                self.children[-1].link_to(element)
            self.add(element)

        if len(self.children):
            if not self.children[0].context or\
               not self.context.add_pad(Gst.GhostPad.new("sink", self.children[0].context.get_static_pad("sink"))):
                print("Error to setup: error={}".format(__class__, sys.exc_info()[0]))

            if not self.children[-1].context or\
               not self.context.add_pad(Gst.GhostPad.new("src", self.children[-1].context.get_static_pad("src"))):
                print("Error to setup: error={}".format(__class__, sys.exc_info()[0]))

        return self

    def _nvdsanalytics_save_config_file(self, path, model, job):
        pass

class SourceElement(GstElement):
    def __init__(self, pipeline, factory_name=None, name=None):
        GstElement.__init__(self, pipeline,
                        factory_name if factory_name else "uridecodebin",
                        name if name else "source-bin")

    def setup(self, model, job):
        if job:
            self.set_property("uri", job.uri)
        return self


class NvVideoConvert(GstElement):
    def __init__(self, pipeline, factory_name=None, name=None):
        GstElement.__init__(self, pipeline,
                            factory_name if factory_name else "nvvideoconvert",
                            name if name else "scale-crop")

    def setup(self, model, job):
        return self


class NvStreamMux(GstElement):
    def __init__(self, pipeline, factory_name=None, name=None):
        GstElement.__init__(self, pipeline,
                            factory_name if factory_name else "nvstreammux",
                            name if name else "Stream-muxer")

    def setup(self, model, job):
        self.set_property("width", model.streammux_width)
        self.set_property("height", model.streammux_height)
        # TODO change below 2 lines to get from time to create source-bins or commandline or filesystem
        self.set_property("batch-size", 1)
        self.set_property("batched-push-timeout", 100)
        self.set_property("nvbuf-memory-type", int(pyds.NVBUF_MEM_DEFAULT)) # or int(pyds.NVBUF_MEM_CUDA_UNIFIED)
        return self


class FakeSink(GstElement):
    def __init__(self, pipeline, factory_name=None, name=None):
        GstElement.__init__(self, pipeline,
                            factory_name if factory_name else "fakesink",
                            name if name else "sink")

    def setup(self, model, job):
        self.set_property("sync", 0)
        return self


if __name__ != "__main__":
    Gst.init()
