from inference.runners._mapping import RUNNERS
import sys

__all__ = ['get_runner_by_name']


def get_runner_by_name(name, **options):
    if name not in RUNNERS:
        raise IOError("unsupported name: {}" % name)

    module_name, name = RUNNERS[name]
    mod = __import__(module_name, None, None, ['__all__'])
    return getattr(mod, name)(**options)
