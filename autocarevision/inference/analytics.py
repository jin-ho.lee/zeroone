import argparse
import json
import netifaces
from os.path import abspath, dirname
import pathlib
import sys
import uuid

import inference.runners


class Analytics:
    def __init__(self, argv, *args, **kwargs):
        self._profile = None
        self._runners = []
        self._state = None

        try:
            self._profile = Profile(argv, *args, **kwargs)
            self._set_state("Registered")
        except:
            self._set_state("Broken")

    @property
    def profile(self):
        return self._profile

    @property
    def state(self):
        return self._state

    def assign(self, **params):
        runner = None
        try:
            runner = Runner.create()
            runner.init()
            runner.config(self.profile.model, None)
            self._runners.append(runner)
            self._set_state("Assign")
        except:
            self._set_state("Broken")
        return runner

    def _set_state(self, trg):
        if self._profile:
            print("Analytics<{}.{}> stateChanged: {} -> {}".
                  format(self._profile.name, self._profile.id, self._state, trg))
        if not self._profile:
            print("Analytics<> stateChanged: {} -> {}".format(self._state, trg))
        self._state = trg

    @classmethod
    def create(cls, **kwargs):
        return Analytics(**kwargs)


class Profile:
    def __init__(self, argv, *args, **kwargs):
        self._id = uuid.UUID(int=0)
        self._name = None
        self._address = None
        self._models = []
        self._model = None

        # initialize system-specific variables
        try:
            self._name = sys.argv[0].split("/")[-1]
            ni_name = netifaces.gateways()["default"][netifaces.AF_INET][1]  # network interface name
            self._address = netifaces.ifaddresses(ni_name)[netifaces.AF_INET][0]["addr"]
        except:
            raise RuntimeError(
                "Error for {} initialization of feature-specific: {}".
                format(__class__, sys.exc_info()[0]))

        # initialize feature-specific variables
        try:
            parser = argparse.ArgumentParser()
            parser.add_argument("--models-path", type=str)
            parser.add_argument("--models-file", type=str)
            parsed = parser.parse_args(args=argv[1:])
            if getattr(parsed, "models_path"):
                self._models = Model.load_at_path(parsed.models_path)
            elif getattr(parsed, "models_file"):
                self._models = Model.load_from_file(parsed.models_file)
            self._model = self._models[0]
        except:
            raise RuntimeError(
                "Error for {} initialization of feature-specific: {}".
                format(__class__, sys.exc_info()[0]))

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def address(self):
        return self._address

    @property
    def models(self):
        return self._models

    @property
    def model(self):
        return self._model


class Job:
    def __init__(self, **kwargs):
        self._state = None
        self._cmd = None
        self._id = None
        self._stream = None
        self._name = None
        self._app = None
        self._uri = None
        self._settings = {}

    @property
    def state(self):
        return self._state

    @property
    def cmd(self):
        return self._cmd

    @property
    def id(self):
        return self._id

    @property
    def stream(self):
        return self._stream

    @property
    def name(self):
        return self._name

    @property
    def app(self):
        return self._app

    @property
    def uri(self):
        return self._uri

    @property
    def settings(self):
        return self._settings

    def update(self, **params):
        pass


class Model:
    def __init__(self, name=None, attrs=None):
        self._name = name
        self.__dict__ = attrs



    @classmethod
    def resolve_path_recursive(cls, root, attrs):
        if isinstance(attrs, list):
            for key, value in enumerate(attrs):
                if isinstance(value, dict) or isinstance(value, list):
                    Model.resolve_path_recursive(root, value)
                elif not isinstance(value, str):
                    continue
                else:
                    path = pathlib.Path("/".join([dirname(root), value])).resolve()
                    if not path.is_file():
                        continue
                    attrs[key] = path
        elif isinstance(attrs, dict):
            for key, value in attrs.items():
                if isinstance(value, dict) or isinstance(value, list):
                    Model.resolve_path_recursive(root, value)
                elif not isinstance(value, str):
                    continue
                else:
                    path = pathlib.Path("/".join([dirname(root), value])).resolve()
                    if not path.is_file():
                        continue
                    attrs[key] = str(path)
        else:
            raise RuntimeError("Error for {} to resolve path recursive: {}".
                               format(__class__, sys.exc_info()[0]))
        return attrs

    @classmethod
    def load_from_file(cls, file):
        with open(abspath(file)) as fd:
            jo = json.load(fd)
            models = [Model(name=name, attrs=Model.resolve_path_recursive(file, jo[name])) for name in jo]
        return models

    ##TODO add to load models information at path
    @classmethod
    def load_at_path(cls, path):
        return []


class Runner:
    def __init__(self, job=None, model=None):
        self._model = model
        self._job = job

    @property
    def model(self):
        return self._model

    @property
    def job(self):
        return self._job

    def init(self):
        pass

    def config(self, model, job):
        pass

    def run(self):
        pass

    def stop(self):
        pass

    @classmethod
    def create(cls, **kwargs):
        return inference.runners.get_runner_by_name("deepstream", **kwargs)
