from core.logger import init_logger
from core.task import *
from app.task import *

import argparse


if __name__ == "__main__":
    # TODO make a manual for input commandline including help messages
    parser = argparse.ArgumentParser()
    parser.add_argument("--loglevel", type=str)  # --loglevel=[debug|info|warning|error|critical]
    parser.add_argument("--logfile", type=str)  # --logfile=[file path]
    parsed, _ = parser.parse_known_args(args=sys.argv[1:])

    # init logger
    init_logger(parsed.loglevel, parsed.logfile)

    # begin all tasks using delegator
    delegator = TaskDelegator([
                   PresentationTaskBranch(sys.argv, mode=TaskBranch.Asyncio),
                   InferenceTaskBranch(sys.argv),
                   NetworkTaskBranch(sys.argv)])
    delegator.exec()
